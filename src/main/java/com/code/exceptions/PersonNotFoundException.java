package com.code.exceptions;

import java.text.MessageFormat;

public class PersonNotFoundException extends Exception{

    private static final String MESSAGE_EXCEPTION = "Person with id {0} not found!";

    public PersonNotFoundException(Integer id){
        super(MessageFormat.format(MESSAGE_EXCEPTION, id));
    }
}
