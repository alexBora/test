package com.code.controllers;

import com.code.dtos.PersonDTO;
import com.code.exceptions.PersonNotFoundException;
import com.code.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/persons")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping(path = "/{id}")
    public @ResponseBody
    ResponseEntity getPersonById(@PathVariable("id") Integer id){
        try{
            PersonDTO personDTO = personService.getPersonById(id);
            return new ResponseEntity<>(personDTO, HttpStatus.OK);
        }catch(PersonNotFoundException ex){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity addPerson(@RequestBody PersonDTO personDTO){
            int ageOfNewPerson = personService.addNewPerson(personDTO);
            return new ResponseEntity<>(ageOfNewPerson, HttpStatus.OK);
    }

}
