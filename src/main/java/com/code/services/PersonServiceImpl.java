package com.code.services;

import com.code.dtos.PersonDTO;
import com.code.dtos.mappers.PersonMapper;
import com.code.entities.Person;
import com.code.exceptions.PersonNotFoundException;
import com.code.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService{

    @Autowired
    private PersonRepository personRepository;

    private PersonMapper personMapper = PersonMapper.INSTANCE;

    @Override
    public PersonDTO getPersonById(Integer id) throws PersonNotFoundException {
        Optional<Person> personFromDB = personRepository.findById(id);
        if(!personFromDB.isPresent()){
            throw new PersonNotFoundException(id);
        }
        return personMapper.mapToDTO(personFromDB.get());
    }

    @Override
    public Integer addNewPerson(PersonDTO personDTO) {
        return personRepository.save(personMapper.mapToEntity(personDTO)).calculateAge();//getId();
    }
}
