package com.code.services;

import com.code.dtos.PersonDTO;
import com.code.exceptions.PersonNotFoundException;

public interface PersonService {

    PersonDTO getPersonById(Integer id) throws PersonNotFoundException;

    Integer addNewPerson(PersonDTO personDTO);
}
