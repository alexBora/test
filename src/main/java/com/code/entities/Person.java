package com.code.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.Period;

@Entity
public class Person {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private LocalDate birthdate;

    public int calculateAge() {
        if (birthdate != null) {
            return Period.between(birthdate, LocalDate.now()).getYears();
        } else return 0;
    }

    public Integer getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }
}
