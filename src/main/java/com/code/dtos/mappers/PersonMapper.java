package com.code.dtos.mappers;

import com.code.dtos.PersonDTO;
import com.code.entities.Person;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PersonMapper {

    PersonMapper INSTANCE = Mappers.getMapper(PersonMapper.class);

    Person mapToEntity(PersonDTO personDTO);

    PersonDTO mapToDTO(Person person);
}
