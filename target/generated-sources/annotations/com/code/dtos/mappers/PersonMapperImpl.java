package com.code.dtos.mappers;

import com.code.dtos.PersonDTO;
import com.code.entities.Person;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-12-04T20:54:07+0200",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 1.8.0_211 (Oracle Corporation)"
)
public class PersonMapperImpl implements PersonMapper {

    @Override
    public Person mapToEntity(PersonDTO personDTO) {
        if ( personDTO == null ) {
            return null;
        }

        Person person = new Person();

        person.setName( personDTO.getName() );
        person.setBirthdate( personDTO.getBirthdate() );

        return person;
    }

    @Override
    public PersonDTO mapToDTO(Person person) {
        if ( person == null ) {
            return null;
        }

        PersonDTO personDTO = new PersonDTO();

        personDTO.setName( person.getName() );
        personDTO.setBirthdate( person.getBirthdate() );

        return personDTO;
    }
}
